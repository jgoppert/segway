import sympy

"""
A module to handle sympy Quaternion math.
author: James Goppert
license: GPL v3
"""

i,j,k = sympy.symbols('i,j,k',commutative=False)

use_markley_axioms = False

axioms = {i**2:-1,j**2:-1,k**2:-1,i*j*k:-1,
               i*j:k,j*i:-k,
               j*k:i,k*j:-i,
               i*k:-j,k*i:j}

axioms_markley = {i**2:-1,j**2:-1,k**2:-1,i*j*k:1,
               i*j:-k,j*i:k,
               j*k:-i,k*j:i,
               i*k:j,k*i:-j}

def skew(v):
    return sympy.Matrix([[0, -v[2], v[1]],
                         [v[2], 0, -v[0]],
                         [-v[1], v[0], 0]
                         ])

def create(qs,qv):
    return qs + qv[0]*i + qv[1]*j + qv[2]*k

def simp(q):
    if use_markley_axioms:
        return q.expand().subs(axioms_markley).collect([i,j,k],sympy.factor)
    else:
        return q.expand().subs(axioms).collect([i,j,k],sympy.factor)

def product(p,q):
    return simp(p*q)
   
def conjugate(q):
    return simp(q.subs({i:-i,j:-j,k:-k}))

def norm(q):
    return sympy.sqrt(simp(conjugate(q)*q))

def inverse(q):
    return conjugate(q)/norm(q)**2

def scalar_part(q):
    return simp(((q + conjugate(q))/2))

def vector_part(q):
    return simp((q - scalar(q)))

def to_scalar_vector_arrays(q):
    qs = q.subs({i:0,j:0,k:0})
    qv = sympy.Matrix([(q - q.subs(i,0)).subs(i,1),
     (q - q.subs(j,0)).subs(j,1),
     (q - q.subs(k,0)).subs(k,1)])
    qs.simplify()
    qv.simplify()
    return (qs,qv)

def product_arrays(p,q):
    ps, pv = to_scalar_vector_arrays(p)
    qs, qv = to_scalar_vector_arrays(q)
    if use_markley_axioms:
        return simp(ps*qs -pv.dot(qv) + qs*pv.dot([i,j,k]) + ps*qv.dot([i,j,k]) - pv.cross(qv).dot([i,j,k]))
    else:
        return simp(ps*qs -pv.dot(qv) + qs*pv.dot([i,j,k]) + ps*qv.dot([i,j,k]) + pv.cross(qv).dot([i,j,k]))

def to_dcm(q):
    (qs, qv) = to_scalar_vector_arrays(q)
    return (qs**2 - qv.dot(qv))*sympy.eye(3) - 2*qs*skew(qv) + 2*qv*qv.T

def deriv(q, omega):
    if use_markley_axioms:
        return simp(product(create(0, omega), q)/2)
    else:
        return simp(product(q, create(0, omega))/2)

class Quaternion(object):
    """
    A class, seems easier not to go this route and use functions above
    """
    def __init__(self, scalar, vector):
        self.s = scalar
        self.v = vector
    def __mul__(self, q2):
        q1 = self
        return Quaternion(q1.s*q2.s - q1.v.dot(q2.v), q1.v*q2.s + q2.v*q1.s - q1.v.cross(q2.v))
    def __div__(self, scalar):
        s = sympy.sympify(scalar)
        return Quaternion(self.s/s, self.v/s)
    def __rmul__(self,scalar):
        s = sympy.sympify(scalar)
        return Quaternion(self.s*s, self.v*s)
    def __add__(self, q2):
        q1 = self
        return Quaternion(q1.s+q2.s,q1.v+q2.v)
    def __sub__(self, q2):
        q1 = self
        return Quaternion(q1.s-q2.s,q1.v-q2.v)
    def diff(self, omega):
        q = self
        return q*Quaternion(0,omega)/2
    def __str__(self):
        q = self
        return '[{:s}], [{:s}]'.format(q.s.__str__(),q.v.__str__())
    _sympystr = __str__
    _sympyrepr = _sympystr
    __repr__ = __str__
    #__radd__ = __add__
    #__rand__ = __and__
    #__rmul__ = __mul__    
