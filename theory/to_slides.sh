#!/bin/bash
curdir=$(pwd)
if [ $# != 3 ]
then
    echo "usage: $0 author title notebook"
    exit -1
else
    author=$1
    title=$2
    notebook=$3
fi
rm -rf tmp
mkdir -p tmp
cd tmp
ipython nbconvert --to=slides \
    --post=serve \
    --SphinxTransformer.author="$author" \
    --SphinxTransformer.overridetitle="$title" "$curdir/$notebook"

# vim:ts=4:sw=4:expandtab
