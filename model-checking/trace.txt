-- specification AG !(state = failure)  is false
-- as demonstrated by the following execution sequence
Trace Description: CTL Counterexample 
Trace Type: Counterexample 
-> State: 1.1 <-
  request = back
  state = stop
-> State: 1.2 <-
  request = fwd
  state = back
-> State: 1.3 <-
  state = failure
